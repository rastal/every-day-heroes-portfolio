# Every Day Heroes

A small game about being a normal security guard trying to keep people safe while superheroes are fighting each other and destroying the city. The high concept pitch was "Paul Blart in the Marvel Cinematic Universe."

I wrote pretty much all the logic for this game except for the player controller and HUD.

**[Download on itch.](https://pupcheco.itch.io/every-day-heroes)**